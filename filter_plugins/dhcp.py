KNOWN_MODELS = ("EX3400-48T", "EX4600-40F-AFO", "QFX10002-36Q", "QFX10002-72Q")


def _parse_interface(interface, router, broadcast, ztp):
    if interface.get("model") not in KNOWN_MODELS:
        return None
    result = {
        "name": interface.get("name"),
        "ip": interface.get("ip"),
        "mac": interface.get("mac"),
        "model": interface["model"],
        "router": router,
        "bcast": broadcast,
    }
    if ztp:
        result["ztp"] = ztp
    return result


def csentry_to_dhcp(interfaces, router=None, broadcast=None, ztp=False):
    result = [
        _parse_interface(interface, router, broadcast, ztp) for interface in interfaces
    ]
    return [item for item in result if item]


class FilterModule(object):
    def filters(self):
        return {"csentry_to_dhcp": csentry_to_dhcp}
