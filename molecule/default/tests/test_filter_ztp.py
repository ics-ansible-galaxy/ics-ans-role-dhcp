import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-role-dhcp-filter-ztp*')


def test_ztp_config_and_firmware_present(host):
    conf = host.file("/etc/dhcp/dhcpd.conf").content_string
    assert 'option NEW_OP.config-file-name "ics-infrastructure/ics-juniper-initial-configuration/raw/master/host1.conf";' in conf
    assert 'option NEW_OP.config-file-name "ics-infrastructure/ics-juniper-initial-configuration/raw/master/host2.conf";' in conf
    assert 'option NEW_OP.image-file-name "ics-infrastructure/ics-juniper-initial-configuration/raw/master/EX3400-48T/junos-arm-32-15.1X53-D56.tgz";' in conf


def test_filter_unknown_model(host):
    conf = host.file("/etc/dhcp/dhcpd.conf").content_string
    assert 'host3.conf' not in conf
