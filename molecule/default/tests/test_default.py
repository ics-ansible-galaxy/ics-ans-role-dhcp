import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_dhcpd_enabled_and_started(host):
    if (host.ansible.get_variables()["inventory_hostname"].endswith("ubuntu20")) or (host.ansible.get_variables()["inventory_hostname"].endswith("ubuntu22")):
        service = host.service("isc-dhcp-server")
    else:
        service = host.service("dhcpd")
    assert service.is_running
    assert service.is_enabled
