import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-role-dhcp-filter-no-ztp*')


def test_ztp_config_and_firmware_not_present(host):
    conf = host.file("/etc/dhcp/dhcpd.conf").content_string
    assert 'option NEW_OP.config-file-name "ics-infrastructure' not in conf
    assert 'option NEW_OP.image-file-name "ics-infrastructure' not in conf
