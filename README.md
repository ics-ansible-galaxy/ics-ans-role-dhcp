ics-ans-role-dhcp
=================

Ansible role to install dhcp.

Requirements
------------

- ansible >= 2.5
- molecule >= 2.15

Role Variables
--------------

The dhcp_hosts variable shall be passed to the role.
It can be retrieved from CSEntry using the csentry lookup plugin and csentry_to_dhcp filter.

```yaml
interfaces: "{{ lookup('csentry', 'interfaces', url=csentry_url, token=csentry_token, network_id=3) }}"
dhcp_hosts: '{{ interfaces | csentry_to_dhcp(broadcast="192.168.1.255", router="192.168.1.254", ztp=True) }}'
```

The csentry_to_dhcp filter only returns entries that have a known model:
- EX3400-48T
- EX4600-40F-AFO
- QFX10002-36Q
- QFX10002-72Q

Note that the broadcast and router addresses are passed as parameter to the filter and shall be identical
for all the interfaces. You should only pass a list of interfaces from a specific network.
Retrieving the broadcast and router from CSEntry is possible but requires some extra development.

Note: if ztp is set, it will retrieve the configuration and firmware from `dhcp_ztp_config_path` on
`dhcp_global_http_server`.


```yaml
dhcp_global_default_lease_time: 7776000
dhcp_global_max_lease_time: 7776000
dhcp_global_subnet_mask: SUBNETMASK
dhcp_global_broadcast_addresss: IPADDRESS
dhcp_global_domain_name: DOMAIN
dhcp_global_domain_name_servers:
  - DNSIPADDRESS1
  - DNSIPADDRESS2
dhcp_global_bootp: allow
dhcp_global_booting: allow
dhcp_global_next_server: IPADDRESS
dhcp_global_filename: IMAGEFILE
dhcp_global_wpad: http://CHANGEME/proxy.pac
dhcp_global_log_facility: local7
dhcp_packages:
  - dhcp
dhcp_config_dir: /etc/dhcp
dhcp_config: /etc/dhcp/dhcpd.conf
dhcp_service: dhcpd
dhcp_global_default_lease_time: 7776000
dhcp_global_max_lease_time: 7776000
dhcp_global_subnet_mask: 255.255.255.0
dhcp_global_broadcast_addresss: 172.16.2.255
dhcp_global_domain_name: tc.esss.lu.se
dhcp_global_domain_name_servers:
  - 172.16.2.20
dhcp_global_bootp: allow
dhcp_global_booting: allow
dhcp_global_next_server: 172.16.2.200
dhcp_global_filename: boot.img
dhcp_global_wpad: http://proxy.ics.esss.se/proxy.pac
dhcp_global_log_facility: local7
dhcp_global_http_server: gitlab.esss.lu.se

dhcp_subnets:
  - ip: 0.0.0.0
    netmask: 0.0.0.0
dhcp_hosts:
  - name: DHCPCLIENT1
    mac: 'MACADDRESS'
    ip: IPADDRESS1
    router: ROUTER
    bcast: BROADCAST ADDRESS
    ztp: true #if defined then tftp and NEW_OP options are added - this can be omitted
    model: DEVICE MODEL (only used if ztp is defined)

dhcp_ztp_config_path: "ics-infrastructure/ics-juniper-initial-configuration/raw/master"

dhcp_firmware:
  EX3400-48T: junos-arm-32-15.1X53-D56.tgz
  EX4600-40F-AFO: jinstall-host-ex-4600-17.2R1.13-signed.tgz
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-dhcp
```

How to run
--------------

```
git clone git@icsv-gitlab-02.ics.esss.lu.se:playbooks/ics-ans-role-dhcp.git
vi ics-ans-role-dhcpvars/main.yml
ansible-playbook -i $pathtoinventory ics-ans-role-dhcpvars/playbook.yml
```

Testing
-------
Prepare your molecule environment as described here https://confluence.esss.lu.se/display/DE/Ansible+role+development+workflow
```
molecule create
molecule converge
```
Remember to run the following command after completing your tests
```
molecule destroy
```

License
-------

BSD 2-clause
